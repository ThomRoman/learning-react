# **Learning React**

## **¿Qué es React?**

- Es una libreria de js para crear aplicaciones SPA
- Es una libreria declarativa ya que es abierte a otros paradigmas, **patrones de diseño y arquitecturas
- Es una libreria eficiente
- Es una libreria predecible, nos ayuda a prevenir mutaciones a los largo de un proceso
- Basado en componentes
- Se puede hacer aplicaciones react del lado del servidor con Server-side
- Se puede hacer aplicaciones moviles con React Native

```jsx
    // JSX = JS + XML
    const h1Tag = <h1>Hola Mundo</h1> // esto es  con jsx
    // const h1Tag = document.createElement('h1',null,`Hola mundo ${nombre}`) // esto es con js
    
    // con jsx
    const divRoot = document.querySelector('#root');

    ReactDOM.render(h1Tag,divRoot);
```

## **Componentes en React**

Pequeña pieza de código encapsulada reutilizable que puede tener estado o no. <br/>
Cuando se habla de estado se refiere a la mutacion del componente por interno, las propiedades.Es decir como se encontrara
la informacion del componente en un punto determinado del tiempo

![alt](./resources/components.png)

```js
    // Componentes
    // composicion abstracta de como se verian los componentes
    TwitterApp (Component) > Router (Sirve para la navegacion de paginas sin hacer el refresh) > Paginas > Menu > MenuItem
    // y se ira creando un arbol de componentes en la aplicacion de react


    // como se puede observar un componente puede tener otros componentes
```

![alt](./resources/components-1.png)

## **Fragments**

Permite agrupar nodos hijos sin crear un nodo padre en el DOM, es decir
agrupa elementos de un componente. En algunos casos nos servira poner un div u otro
etiqueta que sirva como container semantico y listo pero en algunos casos solo
necesitaremos agrupar elementos y retornarlos asi sin crear un nodo padre en el DOM como
contenedor

## **HOC - Higher Order Component - Componente_de_orden_mayor**

Reutilizar la lógica de un componente a lo largo de un proyecto y esto se logra concretamente con una función común y corriente que recibe como parámetro un componente y retorna un nuevo componente

## **Comunicación entre componentes - props(properties)**

props son las propiedades que son enviadas a los componentes

## DefaultProps

`Component.defaultProps`
## SynthecticEvent

es un container que forma parte del sistema de evento de React
- https://es.reactjs.org/docs/events.html
- https://es.reactjs.org/docs/handling-events.html

## **Hooks**

- es un funcion
- es un nueva forma de escribir componentes sin la ncesidad de
  escribir clases, ayudando al manejo del estado o el ciclo de vida
- nos serviran para poder reutilizar la logica de un componente gracias a los HOC

### **Tipos de Hooks**

- Incorporados : `useState`, `useContext`, `useEffect`
- Adicionales : `useReducer`, `useCallback`, `useMemo`...
- Otros : hooks personalizados

## **useState and useContext**

- useContext
- useState

```js
const later = (delay, value) => {
    let timer = 0;
    let reject = null;
    const promise = new Promise((resolve, _reject) => {
        reject = _reject;
        timer = setTimeout(resolve, delay, value);
    });
    return {
        get promise() { return promise; },
        cancel() {
            if (timer) {
                clearTimeout(timer);
                timer = 0;
                reject();
                reject = null;
            }
        }
    };
};

const l1 = later(100, "l1");

l1.promise
  .then(msg => { console.log(msg); })
  .catch(() => { console.log("l1 cancelled"); });

const l2 = later(200, "l2");

l2.promise
  .then(msg => { console.log(msg); })
  .catch(() => { console.log("l2 cancelled"); });

setTimeout(() => {
  l2.cancel();
}, 150);

```

```sh
   npx create-react-app my-app
    cd my-app
    npm start
```

## **References**

- [Instalaciones](https://gist.github.com/Klerith/63604bd1443a5d8ca71aa75f58d0e25f)
- [React dev tools](https://addons.mozilla.org/es/firefox/addon/react-devtools/)
- [Redux devtools](https://addons.mozilla.org/es/firefox/addon/reduxdevtools/)
- Extension VSCode - ES7 React/Redux/GraphQL/React-Native snippets
- Extension VSCode - Simple React Snippets
- Extension VSCode - Auto Close Tag
- [ciclo de vida de un componente](https://es.reactjs.org/docs/state-and-lifecycle.html)
- [CDN](https://es.reactjs.org/docs/cdn-links.html)
- https://gist.github.com/Klerith/b0111f52ba16451d095f38d4c995605b
- https://www.freecodecamp.org/news/javascript-new-features-es2020/
- https://create-react-app.dev/docs/getting-started/ (esqueleto para crear un proyecto en React)
- https://dev.to/samanthaming/how-to-deep-clone-an-array-in-javascript-3cig
- https://www.youtube.com/watch?v=NE4sU3_iLDM
- https://javascript.plainenglish.io/how-to-deep-copy-objects-and-arrays-in-javascript-7c911359b089 (object and array)
- https://www.samanthaming.com/tidbits/70-3-ways-to-clone-objects/
- https://stackoverflow.com/questions/22707475/how-to-make-a-promise-from-settimeout
- https://developers.giphy.com/
- https://es.reactjs.org/docs/getting-started.html
- [Fragmentos](https://es.reactjs.org/docs/fragments.html)
- [Componentes de orden superior](https://es.reactjs.org/docs/higher-order-components.html)
- [Higher order component- Edteam](https://ed.team/blog/higher-order-components-en-react)
- Hay una dependencia `rafcp` y me auto completa algunos imports y la functional component ((ES7 React/Redux/GraphQL/React-Native snippets))
- https://www.freecodecamp.org/news/beginners-guide-to-using-react-hooks/
- https://scotch.io/courses/5-essential-react-concepts-to-know-before-learning-redux/presentational-and-container-component-pattern-in-react
- https://www.geeksforgeeks.org/introduction-to-react-hooks/
- https://es.reactjs.org/docs/hooks-custom.html
- https://ed.team/blog/introduccion-los-hooks-de-react
- https://ed.team/blog/conoce-como-reutilizar-codigo-con-los-custom-hooks-de-react
- https://es.reactjs.org/docs/hooks-intro.html
- https://mauriciogc.medium.com/react-hooks-usestate-useeffect-usecontext-parte-i-f8cc8c9cce0f
- https://latteandcode.medium.com/react-hooks-para-optimizar-el-rendimiento-7e4e5ec4894
- https://latteandcode.medium.com/el-hook-usecallback-y-react-memo-87f761733c35
