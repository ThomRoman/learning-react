import { useState } from "react"

export const useForm = (initialState = {}) => {
    const [formValues, setFormValues] = useState({...initialState})


    const reset = () => setFormValues(initialState)

    const handleChangeInput = event => {
        event.preventDefault()
        const {name,value} = event.target
        setFormValues({
            ...formValues,
            [name]:value
        })
    }
    return [formValues,handleChangeInput,reset]
}
