import {
    useEffect,
    useRef,
    useState
} from "react"

// useRef sive para mantener una referencia mutable

export const useFetch = (url) => {
    // para solucionar si el componente se desmonta y quiere obtener el estado
    console.log("useFetch");
    const isMounted = useRef(true)
    const [state, setState] = useState({
        loading: true,
        data: null,
        error: null
    })


    useEffect(() => {
        isMounted.current = true;
        return () => {
            isMounted.current = false;
            console.log('desmontando...')
        };
    }, []);
    
    useEffect(() => {
        console.log("cambio la url");
        isMounted.current = true;
        setState({
            data: null,
            loading: true,
            error: null
        });
        fetch(url)
            .then((resp) => resp.json())
            .then((data) => {
                setTimeout(()=>{
                    if (isMounted.current) {
                        setState({
                            loading: false,
                            error: null,
                            data,
                        });
                    }
                },2000)
            })
            .catch(() => {
                setState({
                    data: null,
                    loading: false,
                    error: 'No se pudo cargar la informacion',
                });
            });
            return ()=>{
                console.log('desmontando...')
                isMounted.current = false;
            }
    }, [url]);

    return state
}
