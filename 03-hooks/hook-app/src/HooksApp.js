import React from 'react'
// import AppComponentExample from './components/10-useContext-Yo/example-2/AppComponent'
import MainApp from './components/10-useContext/MainApp'
// import TodoApp from './components/09-useReducer-Optimizado/TodoApp'
// import Example from './components/10-useContext-Yo/Example'
// import AppComponentExample from './components/10-useContext-Yo/example/AppComponent'
// import ExampleDocsReducer from './components/09-useReducer/ExampleDocsReducer'
// import CallbackHook from './components/06-memo/CallbackHook'
// import Padre from './components/07-tareas-memo/Padre'
// import TodoApp from './components/09-useReducer/TodoApp'
// import CallbackExample from './components/08-callback/CallbackExample'
// import CallbackSet from './components/08-callback/CallbackSet'
// import MemoHook from './components/06-memo/MemoHook'
// import FormWithCustomHook from './components/02-useEffect/FormWithCustomHook'
// import MultipleCustomHooks from './components/03-examples/MultipleCustomHooks'
// import FocusScreen from './components/04-useRef/FocusScreen'
// import RealExampleRef from './components/04-useRef/RealExampleRef'
// import Layout from './components/05-useLayoutEffect/Layout'
// import Memorize from './components/06-memo/Memorize'
// import MemoHttp from './components/07-tareas-memo/MemoHttp'
// import CounterApp from './components/01-useState/CounterApp'
// import CounterWithCustomHook from './components/01-useState/CounterWithCustomHook'
// import SimpleForm from './components/02-useEffect/SimpleForm'

// import './components/09-useReducer/intro-reduce'

const HooksApp = () => {
    return (
        <div>
            {/* <h1>Hola mundo</h1> */}
            {/* <RealExampleRef /> */}
            {/* <Layout /> */}
            {/* <Memorize /> */}
            {/* <MemoHttp /> */}
            {/* <MemoHook /> */}
            {/* <CallbackHook /> */}
            {/* <CallbackExample /> */}
            {/* <CallbackSet /> */}
            {/* <Padre /> */}

            {/* no optizado */}
            {/* <TodoApp />  */}

            {/* optimizado */}
            {/* <TodoApp />  */}
            {/* <ExampleDocsReducer /> */}
            {/* <Example /> */}
            {/* <AppComponentExample /> */}
            <MainApp />
        </div>
    )
}


export {
    HooksApp as default
}
