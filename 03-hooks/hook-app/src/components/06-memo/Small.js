import React from 'react'

// supongamos que este componente haga una peticion http
// y solo si sus properties cambian se volvera a renderizar pero si el componente padre se actualiza y las properties que se le envia
// a este componente <Small /> no cambian entonces este mismo componente no se vuelve reenderizar y se queda con su estado actual

// es decir usara la version memorizada cuando tenga que redibujar algo
const Small = React.memo(({value}) => {
    console.log(`<Small value=${value} />`);
    return (
        <small>
            {value}
        </small>
    )
})

export default Small