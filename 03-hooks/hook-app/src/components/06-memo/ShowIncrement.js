import React from 'react'

const ShowIncrement = ({increment,value = 1}) => {
    console.log(`<ShowIncrement value=${value} /> `);
    return <button
        className="btn btn-outline-primary"
        onClick={()=>increment(value)}
    >
    { ` + ${value}`}
    </button>
}

export default React.memo(ShowIncrement)
