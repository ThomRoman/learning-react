import React, { useState } from 'react'

import { useCounter } from "./../../hooks/useCounter";
import Small from './Small';

const Memorize = () => {

    const {counter,actions : {increment}} = useCounter(10)
    const [show, setShow] = useState(true)


    console.log("Me volvi a llamar <Memorize />");
    return (
        <div className="m-5">
            {/* <h1>Counter : <small>{counter}</small></h1> */}
            <h1>Counter : <Small value={counter} /></h1>
            <hr/>

            <button
                className="btn btn-primary"
                onClick={event=>increment()}
            
            > +1 </button>

            <button className="btn btn-outline-primary m-2"
            onClick={event=>setShow(!show)}
            
            >  show/hide {
                JSON.stringify(show)
            } </button>
            
        </div>

        /**
         * cada boton renderizada nuevamente este componente
         * pero si este componente si un fetch o una llama a una api que ya consumio previamente y solo se quiere cambiar
         * el estado de algo fuera de ese fetch o ese custom hooks que tiene un fetch , se estara reenderizando todo y por ende
         * se hace otra peticion pero esto no es conveniente asi que aqui viene la funcion memo
         * 
         * entonces encapsularemos el componente que no es necesario que se vuelva a reenderizar si el componente padre cambia
         * 
         * React.memo() y solo cambiara si una de sus properties cambia mas no si el componente se actualiza y no cambia la property que le envia
         * 
         */
    )
}

export default Memorize
