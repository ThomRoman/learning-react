import React, { useMemo, useState } from 'react'
import { procesoPesado1,procesoPesado2 } from '../../helpers/Procesos';
import { useCounter } from '../../hooks/useCounter';
import Small from './Small';

const MemoHook = () => {

    const { counter, actions: { increment } } = useCounter(5000)
    const [show, setShow] = useState(true)

    const memoProcesoPesado1 = useMemo(procesoPesado1,[])
    const memoProcesoPesado2 = useMemo(()=>procesoPesado2(counter),[counter]) // si el counter cambia
    // se debe de tener el nuevo resultado retornado memorizado

    console.log("<MemoHook />");

    

    return (
        <div className="m-5">
            <h1>Counter : <Small value={counter} /></h1>
            <hr />

            {/* <p>{procesoPesado1()}</p> */}
            {/* <p>{procesoPesado2(counter)}</p> */}


            <p>{memoProcesoPesado1}</p>
            <p>{memoProcesoPesado2}</p>

            <button
                className="btn btn-primary"
                onClick={event => increment()}

            > +1 </button>

            <button className="btn btn-outline-primary m-2"
                onClick={event => setShow(!show)}

            >  show/hide {
                    JSON.stringify(show)
                } </button>

        </div>

    )
}

export default MemoHook;

/**
 * 
 * el useMemo devuelve un valor memorizado, si el valor dememorizado cambia(si esta en el arreglo de dependencias)
 * entonces se volvera a ejecutar y se memorizara el nuevo valor. Nos ayudara a evitar calculos costosos en cada render
 * 
 * 
 * el useMemo se ejecuta despues del renderizado
 */