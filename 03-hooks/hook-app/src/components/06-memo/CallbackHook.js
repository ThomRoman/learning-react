import React, { useCallback, useState } from 'react'
import ShowIncrement from './ShowIncrement'
import Small from './Small'

const CallbackHook = () => {
    console.log("<CallbackHook />");

    const [state, setstate] = useState(0)
    const [show, setshow] = useState(true)
    
    const increment= useCallback(
        (factor) => {
            setstate(counter=>counter+factor)
        },
        [setstate],
    )


    return (
        <div className="m-5">
            <h1>use Callbackhook</h1>
            <hr/>
            <p>Counter :  <Small  value={state}/> </p>
            <ShowIncrement increment={increment} />
            <ShowIncrement increment={increment} value={10} />
            <button onClick={()=>setshow(!show)} className="btn btn-primary"> show/hide {JSON.stringify(show)} </button>
        </div>
    )
}

export default CallbackHook

/**
 * 
 * recordar el memoization
 * 
 * https://github.com/ThomRoman/GuiaPython/blob/master/repaso3/memoization.py
 * https://www.python-course.eu/python3_memoization.php
 * 
 * 
 * el useCallback,- devuelve un callback memorizado, es decir que memoriza una funcion,mientras que el useMemo memoriza el retorno de una funcion (un valor)
 * 
 * en conclusion memoriza la instancia de una funcion y reacciona cuando sus parametros cambian
 */