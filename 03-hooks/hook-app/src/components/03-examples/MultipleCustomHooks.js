import React from 'react'
import { useCounter } from '../../hooks/useCounter'
import { useFetch } from '../../hooks/useFetch'

// https://breakingbadapi.com/

const MultipleCustomHooks = () => {

    const {counter,actions} = useCounter(1)
    const {loading,data} = useFetch(`https://www.breakingbadapi.com/api/quotes/${counter}`)
    const {author,quote} = !!data && data[0]

    const handleAddQuote = event => {
        actions.increment(1)
    }

    return (
        <div className="p-5">
            <h1>Custom hooks : breaking bad quotes</h1>
            <hr/>
            {loading ? (
                <div className="alert alert-info text-center">
                    Loading ...
                </div>
            ) : (
                <>
                    <blockquote className="blockquote text-end"> 
                        <p className="mb-3">{quote}</p>
                        <footer className="blockquote-footer">{author}</footer>
                    </blockquote>
                </>
            )}

            {!loading && <button className="btn btn-primary" onClick={ handleAddQuote }>Cambiar</button>}
        </div>
    )
}

export default MultipleCustomHooks
