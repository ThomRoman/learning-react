import React from "react";

const Quote = React.memo(({ quote, author }) => {

    console.log("montando <Quote />");
    return (
        <blockquote className="blockquote text-end">
            <p className="mb-3">{quote}</p>
            <footer className="blockquote-footer">{author}</footer>
        </blockquote>
    );
});

export default Quote;
