import React, { useCallback, useMemo, useState } from 'react'
import Small from '../06-memo/Small';
import Hijo from './Hijo';

const Padre = () => {
    const [state, setstate] = useState(0)
    
    const arrFactors = useMemo(() =>[1,2,3,4,5], []) 

    const incrementar = useCallback(
        (factor) => {
            setstate(counter=>counter+ factor)
        },
        [setstate],
    ) 
    console.log("<Padre />");
    return (
        <div className="m-5">
            <p>Counter : <Small value={state} /></p>
            <hr/>
            {
                arrFactors.map(factor=>(
                    <Hijo factor={factor} incrementar={incrementar} key={factor}/>
                ))
            }
        </div>
    )
}

export default Padre
