import React, { useState } from 'react'
import { useCounter } from '../../hooks/useCounter'
import { useFetch } from '../../hooks/useFetch'
import Spinner from './Spinner'
import Quote from './Quote'

const MemoHttp = () => {
    
    console.log("montando <MemoHTTP />");
    const {counter,actions} = useCounter(1)
    const {loading,data} = useFetch(`https://www.breakingbadapi.com/api/quotes/${counter}`)
    const {author,quote} = !!data && data[0]
    const [show, setShow] = useState(true)

    const handleAddQuote = event => {
        actions.increment(1)
    }

    return (
        <div className="p-5">
            <h1>Custom hooks : breaking bad quotes</h1>
            <hr/>
            {loading ? <Spinner /> : <Quote author={author} quote={quote} />}
            <br/>
            {!loading && <button className="btn btn-primary" onClick={ handleAddQuote }>Cambiar</button>}
            <br/>

            <button className="btn btn-outline-primary m-2"
            onClick={event=>setShow(!show)}
            
            >  show/hide {
                JSON.stringify(show)
            } </button>
        </div>
    )
}

export default MemoHttp
