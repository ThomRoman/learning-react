import React from 'react'

const Hijo = React.memo(({incrementar,factor = 1}) => {
    console.log(`<Hijo factor=${factor} />`);
    return (
        <button
        className="btn btn-primary m-2"
        onClick={()=>incrementar(factor)}
        >
            + {factor}
        </button>
    )
})

export default Hijo
