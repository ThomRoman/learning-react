import React, { useEffect, useState } from 'react'
import './effects.css'; 
import Message from './Message';

/**
 * 
 * El useEffect nos ayudara si sucede un efecto secundarios
 */

const SimpleForm = () => {

    const [form, setForm] = useState({
        name:'',
        password:''
    })

    const {name,password} = form

    useEffect(() => {
        console.log("hey");
        return () => {
            // cleanup
        }
    }, [/*input*/])

    useEffect(() => {
        
        console.log("cambio el name");

        return () => {
        }
    }, [name])

    useEffect(() => {
        console.log("cambio el form")
        return () => {
        }
    }, [form])


    const handleChangeInput = event => {
        event.preventDefault()
        const {name,value} = event.target
        setForm({
            ...form,
            [name]:value
        })
    } 
    return (
        <div className="effect-container">
            <h1>useEffect</h1>
            <hr/>
            <form action="#" className="form-group">
                <input type="text" name="name" className="form-control"
                    autoComplete="off"
                    placeholder="Nombre"
                    onChange={handleChangeInput}
                    value={name}
                />
                <input type="password" name="password" className="form-control"
                    autoComplete="off"
                    placeholder="password"
                    onChange={handleChangeInput}
                    value={password}
                />
            </form>

            { (name === '123') && <Message /> }
        </div>
    )
}

export default SimpleForm
