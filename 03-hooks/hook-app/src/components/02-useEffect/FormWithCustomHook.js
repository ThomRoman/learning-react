import { useEffect } from 'react'
import { useForm } from '../../hooks/useForm'
import './effects.css'

const FormWithCustomHook = () => {

    const [ formValues,handleInputChange] = useForm({
        name:'',
        password:'',
        email:''
    })

    const {name,password,email} = formValues

    useEffect(() => {
        console.log('el email cambio')
    }, [email])

    const handleOnSubmit = event =>{
        event.preventDefault()
        console.table(formValues);
    }
    return (
        <div className="effect-container">
            <h1>FormWithCustomHook</h1>
            <hr/>
            <form action="#" className="form-group" onSubmit={handleOnSubmit}>
                <div className="mb-3">
                    <input type="text" name="name" className="form-control"
                        autoComplete="off"
                        placeholder="Nombre"
                        onChange={handleInputChange}
                        value={name}
                    />
                </div>
                <div className="mb-3">
                    <input type="email" name="email" className="form-control"
                        autoComplete="off"
                        placeholder="test@gmail.com"
                        onChange={handleInputChange}
                        value={email}
                    />
                </div>
                <div className="mb-3">
                    <input type="password" name="password" className="form-control"
                        autoComplete="off"
                        placeholder="password"
                        onChange={handleInputChange}
                        value={password}
                    />
                </div>
                <button type="submit" className="btn btn-primary">Guardar</button>
            </form>

        </div>
    )
}

export default FormWithCustomHook
