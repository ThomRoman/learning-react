import React, { useEffect, useState } from 'react'

const Message = () => {

    const [coord, setCoord] = useState({
        clientX: 0,
        clientY: 0
    }) 
    const {clientX,clientY} = coord
    useEffect(() => {
        console.log("componente montado");
        const handleOnMouseMove = event =>{
            const {clientX,clientY} = event
            setCoord({
                clientX,
                clientY
            })
        }
        window.addEventListener('mousemove',handleOnMouseMove)
        return () => {
            console.log("componente desmontado")
            window.removeEventListener('mousemove',handleOnMouseMove)
        }
    }, [])
    return (
        <div>
            <h3>message</h3>
            {'x : '+clientX}
            <br/>
            {'y : '+clientY}
        </div>
    )
}

export default Message
