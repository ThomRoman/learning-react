import React from 'react'

const AlbumOfTheWeek = React.createContext({
    title: 'Pop Food',
    artist: 'Jack Stauber',
    genre: 'Edible Pop', // lol
});

function UserProfile() {
    return (
        <section>
            <h1>Hi I'm Osman and this is my album of the week:</h1>
            <AlbumOfTheWeek.Consumer>
                {album => (
                    <dl>
                        <dt>Title:</dt>
                        <dd>{album.title}</dd>
                        <dt>Artist:</dt>
                        <dd>{album.artist}</dd>
                        <dt>Genre:</dt>
                        <dd>{album.genre}</dd>
                    </dl>
                )}
            </AlbumOfTheWeek.Consumer>
        </section>
    );
}
const Example = () => {
    return (
        <div className="m-5">
            <UserProfile />
        </div>
    )
}

export default Example
