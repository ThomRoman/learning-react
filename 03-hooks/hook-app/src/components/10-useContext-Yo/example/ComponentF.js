import React from 'react'
import { UserConsumer } from './userContext'

const ComponentF = () => {
    return (
        <div style={{  border: "1px solid black",margin : "1.5em" }}>
            <p>ComponentF</p>
            <UserConsumer>
                { (username)=>{
                    return <div> Name - {username} </div>
                }}
            </UserConsumer>

            {/*  si no se quiere utilizar el codigo(Consumer) anterior se puede utilizar el useContext */}
            {/* 
                const user = useContext(UserContext)

                en el render

                <p> Name - {user}</p>
            
            
            
            
             */}
        </div>
    )
}

export default ComponentF
