import React from 'react'
import {UserConsumer} from './userContext'


const ComponentD = () => {
    return (
        <div style={{  border: "1px solid black",margin : "1.5em" }}>
            <p>ComponentD</p>
            <UserConsumer>
                { (username)=>{
                    return <div> Name - {username} </div>
                }}
            </UserConsumer>
        </div>
    )
}

export default ComponentD
