import React from 'react'
import ComponentE from './ComponentE'

const ComponentC = () => {
    return (
        <div style={{  border: "1px solid black",margin : "1.5em" }}>
            <p>ComponentC</p>
            <ComponentE />
        </div>
    )
}

export default ComponentC
