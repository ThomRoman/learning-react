import React from 'react'
import { UserConsumer } from './userContext'

const ComponentA = () => {
    return (
        <div style={{  border: "1px solid black",margin : "1.5em" }}>
            <p>ComponentA</p>
            <UserConsumer>
                {
                    (username)=> {
                        return <p> Name - {username} </p>
                    }
                }
            </UserConsumer>
        </div>
    )
}

export default ComponentA
