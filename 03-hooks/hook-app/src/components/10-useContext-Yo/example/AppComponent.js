import React from 'react'
import ComponentA from './ComponentA'
import ComponentB from './ComponentB'
import ComponentC from './ComponentC'
import { UserProvider } from './userContext'

const AppComponentExample = () => {
    const value = "Rafael"
    return (
        <div className="m-5" style={ { border:"1px solid red"}}>
            <p>{value}</p>
            <UserProvider value={value}>
                <ComponentA />
                <ComponentB />
                <ComponentC />
            </UserProvider>
        </div>
    )
}

export default AppComponentExample
