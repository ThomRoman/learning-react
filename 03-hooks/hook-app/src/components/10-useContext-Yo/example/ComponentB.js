import React from 'react'
import ComponentD from './ComponentD'

const ComponentB = () => {
    return (
        <div style={{  border: "1px solid black",margin : "1.5em" }}>
            <p>ComponentB</p>
            <ComponentD />
        </div>
    )
}

export default ComponentB
