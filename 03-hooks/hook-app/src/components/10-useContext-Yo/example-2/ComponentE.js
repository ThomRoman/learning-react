import React from 'react'
import ComponentF from './ComponentF'

const ComponentE = () => {
    return (
        <div style={{  border: "1px solid black",margin : "1.5em" }}>
            <p>ComponentE</p>
            <ComponentF />
        </div>
    )
}

export default ComponentE
