import React, { useReducer } from 'react'
import ComponentA from './ComponentA'
import ComponentB from './ComponentB'
import ComponentC from './ComponentC'

const initialState = 0
export const CounterContext = React.createContext(initialState)

const reducer = (state,action)=>{
    switch(action.type){
        case 'increment':
            return state + 1
        case 'decrement':
            return state - 1
        case 'reset' :
            return initialState
        default:
            return state
    }
}

const AppComponentExample = () => {
    const [count,dispacher] = useReducer(reducer,initialState)

    return (
        <div className="m-5" style={ { border:"1px solid red"}}>
            <p>AppComponentExample</p>
            <p>{count}</p>
            <CounterContext.Provider value={ { counterValue:count,counterDispacher:dispacher } }>
                <ComponentA />
                <ComponentB />
                <ComponentC />
            </CounterContext.Provider>
        </div>
    )
}

export default AppComponentExample
