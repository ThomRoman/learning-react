import React, { useContext } from 'react'
import { CounterContext } from './AppComponent'

const ComponentD = () => {
    const counterContext = useContext(CounterContext)
    return (
        <div style={{  border: "1px solid black",margin : "1.5em" }}>
            <p>ComponentD</p>
            <p> {counterContext.counterValue} </p>
            <button
                onClick={ ()=>counterContext.counterDispacher({
                    type: 'increment'
                }) }
            >Incrementar</button>
            <button
                onClick={ ()=>counterContext.counterDispacher({
                    type : 'decrement'
                }) }
            >Decrementar</button>
            <button
                onClick={ ()=>counterContext.counterDispacher({
                    type:'reset'
                })}
            >Reset</button>
        </div>
    )
}

export default ComponentD
