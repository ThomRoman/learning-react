import React, { useContext } from 'react'
import { CounterContext } from './AppComponent'

const ComponentA = () => {
    const counterContext = useContext(CounterContext)
    return (
        <div style={{  border: "1px solid black",margin : "1.5em" }}>
            <p>ComponentA</p>
            <p> {counterContext.counterValue} </p>
            <button
                onClick={ ()=>counterContext.counterDispacher({
                    type: 'increment'
                }) }
            >Incrementar</button>
            <button
                onClick={ ()=>counterContext.counterDispacher({
                    type : 'decrement'
                }) }
            >Decrementar</button>
            <button
                onClick={ ()=>counterContext.counterDispacher({
                    type:'reset'
                })}
            >Reset</button>
        </div>
    )
}

export default ComponentA
