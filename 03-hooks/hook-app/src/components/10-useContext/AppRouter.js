import React from 'react'

import {
    BrowserRouter as Router,
    Switch,
    Route,
    // Redirect,
} from "react-router-dom";
import AboutScreen from './AboutScreen';
import HomeScreen from './HomeScreen';
import LoginScreen from './LoginScreen';
import NavBar from './NavBar';

// este componente se encargara de trabajar y mostrar las rutas

const AppRouter = () => {
    return (
        <Router>
            <NavBar />
            <div>
                <Switch>
                    <Route exact path="/about" component={ AboutScreen } />

                    {/* esto no se puede poner primero ya que hace match con la url ya que busca 
                    la primera coincidencia, por esa razon debe estar al ultimo  */}

                    {/* si quieremos que el path coincida de manera exacta ponemos el atributo exact */}
                    <Route exact path="/" component={ HomeScreen } />
                    <Route exact path="/login" component={ LoginScreen } />

                    {/* si no existe la url, es como el default */}
                    <Route component={ HomeScreen }/>
                    {/* <Route component={ PageNotFound }/> */}
                    {/* pero tambien se puee usar el Redirect */}
                    {/* <Redirect to="/" /> */}
                </Switch>
            </div>
        </Router>
    )
}

export default AppRouter
