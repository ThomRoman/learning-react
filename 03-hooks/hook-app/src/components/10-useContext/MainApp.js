import React, { useState } from 'react'
import AppRouter from './AppRouter'

import {UserContext} from './UserContext'
// import {
//     BrowserRouter as Router,
//     Switch,
//     Route,
//     Link
//   } from "react-router-dom";
// import AboutScreen from './AboutScreen';
// import HomeScreen from './HomeScreen';
const MainApp = () => {


    const [user, setUser] = useState({});
    return (
        <div>
            <h1>MainApp</h1>
            <hr/>

            <UserContext.Provider
                value={ {user,setUser} }
            >
                <AppRouter />

            </UserContext.Provider>
            {/* <Router>
                <main>
                    <AppRouter />
                </main>
            </Router> */}

        </div>
    )
}

export default MainApp
