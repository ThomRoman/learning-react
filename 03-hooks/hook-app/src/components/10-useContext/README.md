# **useContext**

Nos ayuda para la propagación de estados o de acciones que pasamos de padres -> hijos -> nietos -> etc
mediante los props de cada Functional Component. Este API nos ayudará a encapsular el contexto y no será necesario
ir pasando de un nodo superior a un nodo profundo en el Virtual DOM.

> Nos permite encapsular un pedazo de estado en un contexto que es inyectable en cualquier lugar de nuestro árbol de componentes

> El contexto proporciona una forma de pasar datos a través del árbol de componentes sin tener que pasar los accesorios manualmente en todos los niveles.

Para poder usar este hook debemos dominar cuatro conceptos o tres caracteristicas

- React.createContext : creamos un contexto
- MyContext.Provider : con esto encapsulamos un pedazo de un grafo donde cualquiera dentro de esto tendra acceso a los datos del context
- MyContext.Consumer : Con esto consumimos el contexto
- useContext

![Provider](./3tf92ivo.jpg)

<br>
<br>

[Ejemplo de useContext](./../10-useContext-Yo/example/)
![alt](./haqrov0x.jpg)

<br>
<br>

[Ejemplo de useContext y useReducer](./../10-useContext-Yo/example-2/)
![alt](./9lk59wu1.jpg)

## **Guia para usar el useContext con Router**

- https://reactrouter.com/web/guides/quick-start

```sh
    $ npm install react-router-dom
```

## **Referencias**

- [Manejo de estado con Context y Hooks en React](https://medium.com/noders/manejo-de-estado-con-context-y-hooks-en-react-7adfc7a740a3)
