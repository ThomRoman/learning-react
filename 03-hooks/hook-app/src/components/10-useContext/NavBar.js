import React from 'react'
import { Link, NavLink } from 'react-router-dom'

const NavBar = () => {
    return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="container-fluid">
                    {/* <a className="navbar-brand" href="#">Navbar</a> */}
                    <Link to="/" className="navbar-brand" >Navbar</Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                {/* <a className="nav-link active" aria-current="page" href="#">Home</a> */}
                                <NavLink exact activeClassName="active" to="/" className="nav-link">Home</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink exact activeClassName="active" to="/login" className="nav-link">Login</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink exact activeClassName="active" to="/about" className="nav-link">About</NavLink>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
    )
}

export default NavBar

/**
 * 
 * Diferencia entre Link y NavLink
 * 
 * la diferencia es que en link no se sabra en que ruta me encuentro actualmente para activar algun
 *  estilo css, pero en navlink si podemos con el atributo activeClassName
 * y para que actue el estilo solo en la ruta especifica con el exact
 */