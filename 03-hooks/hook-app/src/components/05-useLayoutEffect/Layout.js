import React, { useLayoutEffect, useRef, useState } from 'react'
import { useFetch } from '../../hooks/useFetch'
import {useCounter} from './../../hooks/useCounter'
import './layout.css'
// useLayoutEffect nos permitira obtener propiedades de un elemento del virtual dom despues de
// ser rederizado

const Layout = () => {
    
    const {counter,actions} = useCounter(1)
    const {loading,data} = useFetch(`https://www.breakingbadapi.com/api/quotes/${counter}`)
    const {author,quote} = !!data && data[0]


    const refParrafo = useRef()
    const [boxSize, setBoxSize] = useState({})

    useLayoutEffect(() => {
        console.log("hey");
        if(!loading) {
            console.log(refParrafo.current.getBoundingClientRect());
            setBoxSize(refParrafo.current.getBoundingClientRect())
        }
        return ()=>{

        }
    }, [quote,loading])

    const handleAddQuote = event => {
        actions.increment()
    }

    return (
        <div className="p-5">
            <h1>Custom hooks : breaking bad quotes</h1>
            <hr/>
            {loading ? (
                <div className="spinner">
                </div>
            ) : (
                <>
                    <blockquote className="blockquote text-end"> 
                        <p className="mb-3">{quote}</p>
                        <footer className="blockquote-footer">{author}</footer>
                    </blockquote>
                    <p className="custom-border" ref={refParrafo}>{quote}</p>
                    <pre>
                        {JSON.stringify(boxSize,null,3)}
                    </pre>
                    <br/>
                </>
            )}

            {!loading && <button className="btn btn-primary" onClick={ handleAddQuote }>Cambiar</button>}
        </div>
    )
}

export default Layout
