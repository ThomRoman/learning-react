import React from 'react'
import { useCounter } from '../../hooks/useCounter';

import "./counter.css";

const CounterWithCustomHook = () => {

    const { state,actions } = useCounter(15)

    const handleBtnAction = event =>{
        const { action } = event.target.dataset
        actions[action]();
    } 

    return (
        <div className="container">
            <h1>Counter With Hook : { state }</h1>
            <button className="btn btn-primary m-1" onClick={handleBtnAction} data-action="increment"> + 1</button>
            <button className="btn btn-secondary" onClick={handleBtnAction} data-action="decrement"> - 1</button>
            <button className="btn btn-primary m-1" onClick={()=>actions.reset()}>reset</button>
            <br/>
        </div>
    )
}

export default CounterWithCustomHook
