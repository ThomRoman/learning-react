import React, { useState } from 'react'
import './counter.css'

const CounterApp = () => {
    // const [counter, setCounter] = useState(0)
    const [{counter1,counter2}, setCounter] = useState({
        counter1:10,
        counter2:20,
    })
    const handleCounterAdd = event=>{
        // setCounter(counter + 1)
        const {btn} = event.target.dataset
        switch (btn) {
            case "counter1":
                setCounter(obj=>({...obj,counter1:counter1+1}))
                break;    
            default:
                setCounter(obj=>({...obj,counter2:counter2+1}))
                break;
        }
    }
    return (
        <div className="container">
            <h1>Counter1 { counter1 }</h1>
            <h1>Counter2 { counter2 }</h1>
            <hr/>
            <button className="btn btn-primary"  onClick={handleCounterAdd} data-btn="counter1" > +1 al counter1  </button>
            <button className="btn btn-primary m-2"  onClick={handleCounterAdd} data-btn="counter2"> +1 al counter2  </button> 
        </div>
    )
}

export default CounterApp
