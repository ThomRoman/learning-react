const initialState = [
    {
        id:1,
        todo : 'Comprar pan',
        done:false,
    }
]

const todoReducer = (state = initialState,action=null) => {
    if(action?.type === 'add')
        return [...state,action.payload]
    return state
}

let todos = todoReducer()


const newTodo = {
    id:2,
    todo : 'sacarle la mrd a Rafael David Mendoza Delgado',
    done:false,
}

const addTodoAction = {
    type : 'add',
    payload : newTodo
}

todos = todoReducer(todos,addTodoAction)
console.log("🚀 ~ file: intro-reduce.js ~ line 30 ~ todos", todos)

// https://www.freecodecamp.org/news/javascript-new-features-es2020/