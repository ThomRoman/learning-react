import React, { useEffect, useReducer } from 'react'
import { useForm } from '../../hooks/useForm'
import { reducer } from './todoReducer'

import './useReducer.css'

const initialState = [
    {
        id: new Date().getTime(),
        desc: 'Sacarle la mrd a Rafa',
        done: false
    }
]

const init = () => {
    return  JSON.parse(localStorage.getItem('todos')) || initialState
}

const TodoApp = () => {
    console.log("<TodoApp />");
    const [formValues, handleInputChange,resetValuesForm] = useForm({
        description: ''
    })

    const [state, dispatch] = useReducer(reducer, initialState, init) // la funcion init nos dira que al crear el reduce el valor de initialState d¿se cargara con  el retorno de init

    const handleAddSubmit = event => {
        event.preventDefault()
        const { description } = formValues
        if (description.trim().length <= 0) {
            document.getElementById('input').focus()
            return;
        }

        const newTodo = {
            id: new Date().getTime(),
            desc: description,
            done: false,
        };
        const action = {
            type: 'add',
            payload: newTodo,
        };
        dispatch({ ...action })
        resetValuesForm()
        document.getElementById('input').focus()
    }

    const handleDeleteItem = id => {
        const action = {
            type : 'delete',
            payload : {
                id
            }
        }
        dispatch(action)
    }

    const handleToggle = id => {
        const action = {
            type : 'toggle',
            payload : {
                id
            }
        }
        dispatch(action)
    }

    useEffect(() => {
        localStorage.setItem('todos', JSON.stringify(state));
    }, [state]);

    return (
        <div className="m-5">
            <h1>TodoApp ( { state.length } )</h1>
            <hr/>
            <div className="row">
                <div className="col-7">
                    <ul className="mt-4 list-group list-group-flush">
                        {
                            state.map((todo,index) => <li key={todo.id} data-id={todo.id} className="list-group-item" > 
                                <p className={ todo.done ? "complete" : ""} onClick={()=>handleToggle(todo.id)}> {index+1}. {todo.desc}</p>
                                <button className="btn btn-danger" onClick={ ()=>handleDeleteItem(todo.id)}>Eliminar</button>
                            </li>)
                        }
                    </ul>
                </div>
                <div className="col-5">
                    <h4>Agregar TODO</h4>
                    <hr/>
                    <form onSubmit={handleAddSubmit} style={{ 
                        display:"flex",
                        flexDirection:"column"
                    }}>
                        <input
                            id="input"
                            className="form-control"
                            type="text"
                            name="description"
                            placeholder="Tarea a rercordar..."
                            autoComplete="off"
                            value={formValues.description}
                            onChange={handleInputChange}
                        />
                        <button type="submit" className="btn btn-outline-primary mt-2 btn-block">
                        Agregar
                        </button>
                    </form>
                </div>
            </div>
            
        </div>
    )
}

export default TodoApp
