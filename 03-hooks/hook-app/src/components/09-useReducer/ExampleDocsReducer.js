import React, { useReducer } from 'react'

const reducer = (state,action) => {
    switch(action?.type){
        case "increment":
            return {count : state.count + 1};
        case "decrement":
            return {count : state.count - 1};
        case "reset":
            return {...action.payload};
        default:
            throw new Error();
    }
}

const ExampleDocsReducer = () => {
    console.log("<ExampleDocsReducer />");
    const initialState = {
        count : 0
    }
    const [state, dispatch] = useReducer(reducer, initialState)
    
    return (
        <div className="m-5">
            <p>Counter {  state.count }</p>
            <button onClick={ ()=>dispatch({
                type:"reset",
                payload:{...initialState}
            }) }>Reset</button>
            <button onClick={ ()=>dispatch({type:"increment"}) } > +1 </button>
            <button onClick={ ()=>dispatch({type:"decrement"}) }> -1 </button>
        </div>
    )
}



export default ExampleDocsReducer
