# **Reducer**

- No es mas que una funcion comun y corriente
- Debe ser una funcion pura
  - No debe de tener ejectos secundarios
  - No debe de tener tareas asincronas
  - Debe retornar siempre un nuevo estado
  - no debe de llamar el localstorage o el sessionstorage
  - para modeficar el state no debe de requerir mas que una accion y esta accion puede tener o no argumentos
- Debe retornar siempre un nuevo estado
- usualmente recibe dos argumentos
  - el valor inicial (initial state)
  - y la accion a ejecutar

```js
    const todoReducer = () => {}
```

Tanto useState como useReducer tienen el mismo objetivo: manejar el estado local de nuestros componentes.
La diferencia está en el camino que nos ofrecen para llegar a una solución. No hay ganadores, simplemente hay herramientas y paradigmas que se adaptan mejor a nuestros objetivos. Todo depende del problemas habrá casos donde usar useState será eficiente
y otro en la que no lo serán y se tendra que usar useReducer

El hook useReducer nos permite manejar estados “complejos” por medio de una función reductora.

```js
 // esto es un estado complejo
 {
   foo: {
     faa: {
       test: [],
       testB: ''
     }
   },
   fee: [],
   fii: {
     testC: [],
     testD: {}
   }
 }
```

```js
    const initialTodos = [
        {
            id:1,
            todo: 'Comprar pan',
            done: false
        }
    ]

    const todoReducer = (state = initialTodos,action) => {
        
        return state
    }

    let todos = todoReducer()

    console.log(todos)

    // ¿Para que hacer todo esto?
    /*
    la idea es tener controldado en un solo lugar todas las acciones
    que modifican mi state o mi estado de la aplicacion y de esta forma nosotros
    podemos ver de manera general todas las posibles modificaiones que tiene o puede
    realizar

    */


```

## **Ciclo de vida del reducer**

```js
  // al iniciar la app tendremos un estado inicial

  //state
  [{
    id:1,
    todo:'Comprar pan',
    done:false
  }]

  // entonces el state es un arreglo de objetos

  // el componente se renderiza y muestra el state en la pantalla

  // ahora el usuario necesita agregar un nuevo todo o modificar o eliminar
  // pero la pagina o el componente no hable directamente con el state
  // entonces la pagina creara una accion (Action == add)

  // ahora esta accion se envia al reducer y este tiene todo el mapa
  // de las acciones y cuando termina la accion esto recien modifiara el state
  // el cual al mutar notificara a la vista y se volvera a renderizar

```

![alt](./useReducer.png)
