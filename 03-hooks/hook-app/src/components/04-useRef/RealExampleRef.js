import React, { useState } from 'react'
import MultipleCustomHooks from './../03-examples/MultipleCustomHooks'

const RealExampleRef = () => {

    const [state, setState] = useState({
        show:true,
        message:'Ocultar'
    })

    const {show,message} = state
    return (
        <div className="m-5">
            <h1>Real exmaple ref</h1>
            <hr/>

            {show && <MultipleCustomHooks />}
            <button className="btn btn-outline-primary"  
            
            onClick={()=>setState({
                show:!show,
                message: message === 'Ocultar' ? 'Mostrar' : 'Ocultar' 
            })}
            >{message}</button>
        </div>
    )
}

export default RealExampleRef
