import React, { useRef } from 'react'

const FocusScreen = () => {

    // el uso basico del useRef es para mantener una referencia mutable
    const inputRef = useRef() // por defecto es un objecto {current:undefined}
    // console.log("🚀 ~ file: FocusScreen.js ~ line 7 ~ FocusScreen ~ ref", ref)

    const handleClick = event=>{
        // document.querySelector('input').select()
        inputRef.current.select()
    }

    return (
        <div className="m-5">
            <h1>Focus Screen</h1>
            <div className="mb-3">
                
                <input type="text" name="name" placeholder="Nombre" id="" className="form-control"
                    ref={inputRef}

                />
                
            </div>
            <button className="btn btn-outline-primary"
            onClick={handleClick}
            >Focus</button>
        </div>
    )
}

export default FocusScreen
