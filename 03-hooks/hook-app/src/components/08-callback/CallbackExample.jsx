import React, { useCallback, useMemo, useState } from 'react'

const mostrarCambios = (a,b) =>{
    console.log("🚀 ~ file: CallbackExample.jsx ~ line 4 ~ mostrarCambios ~ a,b", a,b)
}


const CallbackExample = () => {
    console.log("montado <CallbackExample />");
    const [a, setA] = useState(0)
    const [b, setB] = useState(1)
    const [state, setstate] = useState(true)

    const memoization = useMemo(
        () => {
            console.log("cambiaron las dependencias");
            mostrarCambios(a,b)
            return `a : ${a} b : ${b}`
        },
        [a,b],
    )
    const memoizationCallback = useCallback(
        (factor=1,type)=>{
            console.log("memoizationCallback",type);
            if(type === 'a'){

                setA(count=>count + factor)
            }else{
                setB(count=>count + factor)
            }
        },
        [setA,setB]
    )

    const callbackState = useCallback(
        ()=>{
            console.log("callback state");
            setstate(!state)
        },[state]
    )
    return (
        
        <div className="m-5">
            <pre>
                {JSON.stringify(useMemo(()=>{
                    console.log("state")
                    return state
                },[state]))}
            </pre>
            <h1>{memoization}</h1>
            {}
            <br/>
            <button onClick={()=>memoizationCallback(1,'a')}>a</button>
            <button onClick={()=>setB(num=>num+1)}>b</button>
            {/* <button onClick={()=>setstate(!state)}>cambiar</button> */}
            <button onClick={()=>callbackState()}>cambiar</button>
        </div>
    )
}

export default CallbackExample
