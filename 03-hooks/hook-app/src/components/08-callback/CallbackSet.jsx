import React, { useCallback, useState } from 'react'

const functions = new Set();

export default function CallbackSet() {
    const [counter1, setCounter1] = useState(0);
    const [counter2, setCounter2] = useState(0);

    // const increment1 = () => {
    //     setCounter1(counter1 + 1);
    // };

    // const increment2 = () => {
    //     setCounter2(counter2 + 1);
    // };

    const increment1 = useCallback(
        (num) => {
            setCounter1(data=>data + num);
        },
        [setCounter1],
    );
    const increment2 = useCallback(
        (num) => {
            setCounter2(data=>data + num);
        },
        [setCounter2],
    );


    // Register the functions
    functions.add(increment1);
    functions.add(increment2);

    return (
        <div>
            <div> Counter 1 is {counter1} </div>
            <div> Counter 2 is {counter2} </div>
            <br />
            <div>
                <button onClick={increment1}>Increment Counter 1</button>
                <button onClick={increment2}>Increment Counter 2</button>
            </div>
            <br />
            <div> Newly Created Functions: {functions.size} </div>
        </div>
    );
}


// https://mauriciogc.medium.com/react-hooks-usereducer-usecallback-usememo-useref-and-customs-hooks-parte-ii-88950ea413d8
// https://pablomagaz.com/blog/react-hooks-gran-cambio-se-avecina