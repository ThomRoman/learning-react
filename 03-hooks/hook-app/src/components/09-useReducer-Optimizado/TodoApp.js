import React, { useCallback, useEffect, useReducer, useState } from 'react'
import TodoAdd from './TodoAdd'
import TodoList from './TodoList'
import { reducer } from './todoReducer'

import './useReducer.css'

const initialState = [
    {
        id: new Date().getTime(),
        desc: 'Sacarle la mrd a Rafa',
        done: false
    }
]

const init = () => {
    return  JSON.parse(localStorage.getItem('todos')) || initialState
}

const TodoApp = () => {
    console.log("<TodoApp />");
    const [show, setShow] = useState(true)

    const [state, dispatch] = useReducer(reducer, initialState, init) // la funcion init nos dira que al crear el reduce el valor de initialState d¿se cargara con  el retorno de init

    

    // const handleDeleteItem = id => {
    //     const action = {
    //         type : 'delete',
    //         payload : {
    //             id
    //         }
    //     }
    //     dispatch(action)
    // }

    const handleDeleteItem = useCallback(
        (id) => {
            const action = {
                type : 'delete',
                payload : {
                    id
                }
            }
            dispatch(action)
        },
        [dispatch],
    )



    // const handleToggle = id => {
    //     const action = {
    //         type : 'toggle',
    //         payload : {
    //             id
    //         }
    //     }
    //     dispatch(action)
    // }
    const handleToggle = useCallback(
        id => {
            const action = {
                type : 'toggle',
                payload : {
                    id
                }
            }
            dispatch(action)
        },
        [dispatch],
    )

    // const handleAddTodo = todo => {
    //     const action = {
    //         type : 'add',
    //         payload : {...todo}
    //     }
    //     dispatch(action)
    // }
    const handleAddTodo = useCallback(
        todo => {
            const action = {
                type : 'add',
                payload : {...todo}
            }
            dispatch(action)
        },
        [dispatch],
    )

    useEffect(() => {
        localStorage.setItem('todos', JSON.stringify(state));
    }, [state]);

    return (
        <div className="m-5">
            <h1>TodoApp ( { state.length } )</h1>
            <hr/>
            <div className="row">
                <div className="col-7">
                    <TodoList 
                        todos = {state}
                        handleDeleteItem={handleDeleteItem}
                        handleToggle = {handleToggle}    
                    />
                </div>
                <div className="col-5">
                    <TodoAdd 
                        handleAddTodo={handleAddTodo}
                    />
                </div>
            </div>
            <div className="row mt-2">
                <button onClick={ ()=>setShow(!show) } className="btn btn-secondary"> show/hide{JSON.parse(show)} </button>
            </div>
        </div>
    )
}

export default TodoApp
