import React from "react";

const TodoItem = React.memo(({ todo, handleDeleteItem, handleToggle, index }) => {
    console.log("<TodoItem />");
    return (
        <li data-id={todo.id} className="list-group-item">
            <p
                className={todo.done ? "complete" : ""}
                onClick={() => handleToggle(todo.id)}
            > {index + 1}. {todo.desc} </p>
            <button
                className="btn btn-danger"
                onClick={() => handleDeleteItem(todo.id)}
            >
                Eliminar
            </button>
        </li>
    );
});

export default TodoItem;
