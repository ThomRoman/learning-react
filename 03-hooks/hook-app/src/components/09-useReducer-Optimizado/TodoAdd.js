import React from 'react'
import { useForm } from '../../hooks/useForm';

const TodoAdd = React.memo(({handleAddTodo}) => {
    console.log("<TodoAdd />");
    const [formValues, handleInputChange,resetValuesForm] = useForm({
        description: ''
    })
    const handleAddSubmit = event => {
        event.preventDefault()
        const { description } = formValues
        if (description.trim().length <= 0) {
            document.getElementById('input').focus()
            return;
        }

        const newTodo = {
            id: new Date().getTime(),
            desc: description,
            done: false,
        };

        handleAddTodo(newTodo)
        
        resetValuesForm()
        document.getElementById('input').focus()
    }
    return (
        <>
            <h4>Agregar TODO</h4>
            <hr/>
            <form onSubmit={handleAddSubmit} style={{ 
                display:"flex",
                flexDirection:"column"
            }}>
                <input
                    id="input"
                    className="form-control"
                    type="text"
                    name="description"
                    placeholder="Tarea a rercordar..."
                    autoComplete="off"
                    value={formValues.description}
                    onChange={handleInputChange}
                />
                <button type="submit" className="btn btn-outline-primary mt-2 btn-block">
                Agregar
                </button>
            </form>
        </>
    )
})

export default TodoAdd
