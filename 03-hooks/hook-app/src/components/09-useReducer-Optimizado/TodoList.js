import React from 'react'
import TodoItem from './TodoItem'

const TodoList = React.memo(({todos = [], handleDeleteItem, handleToggle}) => {
    console.log(`<TodoList />`);
    return (
        <ul className="mt-4 list-group list-group-flush">
            {
                todos.map((todo,index)=>(
                    <TodoItem 
                        key={todo.id}
                        todo={todo}
                        index={index}
                        handleDeleteItem = {handleDeleteItem}
                        handleToggle = {handleToggle}
                    />
                ))
            }
        </ul>
    )
});

export default TodoList
