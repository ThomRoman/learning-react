# Fugas de memoria cuando se intenta obtener el state de un componente desmontado

- pruebas unitarias
  - en el video de las pruebas del custom hooks de la aplicacion gifexpertapp
  - ```js
        test('debe de retornar el estado inicial', async () => {

        const {result,waitForNextUpdate} = renderHook(()=>useFetchGifs("goku"))
        
        const {data,loading} = result.current
        await waitForNextUpdate()
        expect(data).toEqual([])
        expect(loading).toBe(true)
    })
    ```
- app custom hooks
    - useRef