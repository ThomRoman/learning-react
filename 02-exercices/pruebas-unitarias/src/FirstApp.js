import React from "react"

import PropTypes from 'prop-types'

const FirstApp = ( { saludo,subtitulo = "soy un subtitulo" } ) =>{
    
    const saludo2 = 'Hello world.'
    const obj = {"key":"value"}
    return (
        <>
            <h1>{ saludo2 }</h1>
            <h1>{ saludo } !!</h1>
            <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, sed. Accusamus quam
                
             a deserunt placeat ipsum quaerat iusto voluptate, eius omnis? Ipsa aliquam itaque laboriosam nemo eveniet animi quibusdam vitae?</p>
            <p id="subtitulo" >{ subtitulo }</p>
            <p>{[1,2,3,4]}</p>
            <p>{true}</p>
            <p>{ JSON.stringify(obj)  }</p>  
            <pre>{ JSON.stringify(obj,null,3)  }</pre>  
        </>
    );
}

FirstApp.propTypes = {
    saludo : PropTypes.string.isRequired
}
FirstApp.defaultProps = {
    subtitulo  : "Soy un subtitulo"
}

export {
    FirstApp as default,
}