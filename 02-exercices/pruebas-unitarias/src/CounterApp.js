import React, { useState } from 'react'
import PropTypes from 'prop-types'


const CounterApp = ({value}) =>{

    const [count,setCount] = useState(value)

    const handleAdd = event=>{
        setCount(count + 1)
    }

    const handleSubtract = event =>{
        setCount(count - 1)
    }

    const handleReset = event => {
        setCount(value)
    }

    return (
        <>
            <h1>CounterApp</h1>
            <h2>{ count }</h2>
            <button onClick={ handleAdd }>+1</button>
            <button onClick={ handleReset }>Reset</button>
            <button onClick={ handleSubtract }> -1 </button>
        </>
    );
}
CounterApp.propTypes = {
    value : PropTypes.number,
}

CounterApp.defaultProps = {
    value : 0
}


export {
    CounterApp as default,
}
