import { retornaArreglo } from "../../base/07-deses-arr";


describe('Pruebas en '+__filename, () => {
    test('Debe de retornar un string y un numero', () => {
        // se puede hacer asi tambien o con la destructuracion
        // const arr = retornaArreglo()
        // expect(arr).toEqual(['ABC',123])

        const [letter,number] = retornaArreglo()

        expect(letter).toBe('ABC')
        expect(typeof letter).toBe('string')
        
        expect(number).toBe(123)
        expect(typeof number).toBe('number')
    })
    
})


