// para la autoayuda
import '@testing-library/jest-dom';

import {getSaludo} from './../../base/02-template-string'

describe('Pruebas en '+__filename, () => {
    test('Debe de retornar Hola thom', () => {
        const nombre = 'thom'
        const saludo = getSaludo(nombre)

        expect(saludo).toBe(`Hola ${nombre}`)
    })
    
    // getSaludo debe de reornar Hola Carlos si no hay argumentos
    test('Debe retornar Hola Carlos', () => {
        const saludo = getSaludo()
        expect(saludo).toBe(`Hola Carlos`)
    })
    
})
