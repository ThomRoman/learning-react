import { getHeroeById } from "../../base/08-imp-exp";
import { getHeroeByIdAsync } from "../../base/09-promesas";

import '@testing-library/jest-dom'
describe('Prueba en '+ __filename, () => {
    test('getHeroeByIdAsync debe de obtener un heroe por el id', 
    (done) => {
        const id = 1
        const heroData = getHeroeById(id)
        getHeroeByIdAsync(id)
        .then(res=>{
            expect(res).toEqual(heroData)
            done() // debe determinar la prueba, esto es para
            // pruebas asincronas

        })
    });
    test('getHeroeByIdAsync debe de retornar un error si no existe un heroe por el id', 
    (done) => {
        const id = 10
        getHeroeByIdAsync(id)
        .catch(err=>{
            expect(err).toBe('No se pudo encontrar el héroe')
            done()
        })
    });
})
