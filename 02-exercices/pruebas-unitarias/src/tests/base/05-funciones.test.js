import '@testing-library/jest-dom';
import {getUser,getUsuarioActivo} from '../../base/05-funciones'

describe(`Pruebas en ${__filename}`, () => {
    test('getUser Debe de retornar un objeto '+JSON.stringify({ uid: 'ABC567', username: 'El_Papi1502'}), () => {
        const userTest = {
            uid: 'ABC123',
            username: 'El_Papi1502'
        };

        const user = getUser()
        
        // expect(user).toBe(userTest) // toBe no podra igualar los objetos
        // ya que intentará igualar las referencias {} === {}, esto ultimo retorna false

        expect(user).toEqual(userTest) // toEqual para igualar objetos
    })
    
    test('getUsuarioActivo debe retornar un objeto igual a '+JSON.stringify({ uid: 'ABC567', username: 'thom'}),()=>{
        const userTest = {
            uid: 'ABC567',
            username: 'thom'
        };
        
        const user = getUsuarioActivo('thom')
        expect(user).toEqual(userTest) // toEqual para igualar objetos

    })
})

