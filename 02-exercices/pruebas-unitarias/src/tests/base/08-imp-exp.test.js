
import { getHeroesByOwner,getHeroeById } from '../../base/08-imp-exp';
import heroes from '../../data/heroes';


describe('Prueba en '+ __filename, () => {
    test('getHeroeById debe de retornar un heroe por id', () => {
        const id = 1
        const hero = getHeroeById(id)

        // console.log("🚀 ~ file: 08-imp-exp.test.js ~ line 10 ~ test ~ hero", hero)
        const heroData = heroes.find(hero=>hero.id===id)
        expect(hero).toEqual(heroData)
    })
    test('getHeroeById debe de retornar undefined si hero no existe', () => {
        const id = 10
        const hero = getHeroeById(id)

        // console.log("🚀 ~ file: 08-imp-exp.test.js ~ line 10 ~ test ~ hero", hero)
        expect(hero).toBe(undefined)
    })

    // debe de retornar un arreglo con los heroes de DC
    // toEqual al arreglo filtrado
    test('getHeroesByOwner debe de retornar un arreglo con los heroes de DC', () => {
        const owner = 'DC'
        const heroes = getHeroesByOwner(owner)

        const heroesData = heroes.filter(hero=>hero.owner===owner)
        // const heroesDataisEmpty = !!(heroesData.length && true)

        expect(heroes).toEqual(heroesData)
    })
    test('getHeroesByOwner debe de retornar longitud 2 con los heroes de Marvel', () => {
        const owner = 'Marvel'
        const heroes = getHeroesByOwner(owner)

        // const heroesDataisEmpty = !!(heroesData.length && true)

        expect(heroes.length).toBe(2)
    })
})
