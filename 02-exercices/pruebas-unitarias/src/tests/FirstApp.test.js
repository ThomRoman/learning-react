// import {render} from '@testing-library/react'
// import '@testing-library/jest-dom/extend-expect';
import React from 'react'

import '@testing-library/jest-dom';
import FirstApp from './../FirstApp'
import {  shallow } from "enzyme";

// describe('Pruebas de la FirstApp', () => {
//     test('Debe mostrar el mensaje, "hola"', () => {
//         const saludo = "hola"

//         // producto renderizado que se mostrara en la vista
//         const {getByText} = render( <FirstApp saludo={saludo} />)
//         expect(getByText(saludo)).toBeInTheDocument()
//     })
    
// })


// con enzine
describe('Pruebas de la FirstApp', () => {
    test('Debe mostrar el mensaje, "hola"', () => {
        const saludo = "hola"

        const wrapper = shallow( <FirstApp saludo={saludo} /> )

        expect(wrapper).toMatchSnapshot();
    })
    

    test('Debe de mostrar un subtitulo enviado por props', () => {
        const saludo = "hola"
        const subtitulo = "subtitulo"
        const wrapper = shallow( <FirstApp saludo={saludo} subtitulo={subtitulo} /> )

        // expect(wrapper).toMatchSnapshot();

        // para revisar elementos dentro del componente
        // no olvidarse que el wrapper tiene toda la info
        // del elemento renderizado
    
        // parecido al document.querySelector
        // const textoParrafo = wrapper.find('p')
        const textoParrafo = wrapper
                                .find('#subtitulo')
                                .text()
        console.log("🚀 ~ file: FirstApp.test.js ~ line 48 ~ test ~ textoParrafo", textoParrafo)

        expect(textoParrafo).toBe(subtitulo)
    })
    
})
