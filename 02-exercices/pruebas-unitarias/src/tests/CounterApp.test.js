import React from 'react'
import '@testing-library/jest-dom'
import { shallow } from 'enzyme'
import CounterApp from './../CounterApp'

describe('Pruebas en el componente CounterApp', () => {
    test('el componente CounterApp debe renderizarse con sus valores por defecto', () => {
        const wrapper = shallow( <CounterApp /> )

        expect(wrapper).toMatchSnapshot();
    })

    test('debe de mostrar el valor por defecto de 100', () => {
        const wrapper = shallow( <CounterApp value={100} /> )

        expect(wrapper).toMatchSnapshot();
        const value = wrapper
                                .find('h2')
                                .text().trim()
        
        expect(+value).toBe(100)
    })
    

    test('Debe de incrementar con el boton +1', () => {
        const wrapper = shallow( <CounterApp /> )

        // const btnAdd = wrapper.find('button').at(0)
        // console.log("🚀 ~ file: CounterApp.test.js ~ line 30 ~ test ~ btnAdd.html()", btnAdd.html())
        
        wrapper.find('button').at(0).simulate('click')

        const counterText = wrapper.find('h2').text().trim()
        expect(+counterText).toBe(1)
    })
    
    test('Debe de decrementar con el boton -1', () => {
        const wrapper = shallow( <CounterApp /> )

        // const btnAdd = wrapper.find('button').at(0)
        // console.log("🚀 ~ file: CounterApp.test.js ~ line 30 ~ test ~ btnAdd.html()", btnAdd.html())
        // console.log(wrapper.find('button').at(2).html())
        wrapper.find('button').at(2).simulate('click')

        const counterText = wrapper.find('h2').text().trim()
        expect(+counterText).toBe(-1)
    })

    // si ponemos el wrapper dentro de describe pero fuera
    // de las funciones test
    // las acciones como click se guardaran y x lo tanto
    // el componente cambiara de estado
    // por lo que si en un test se sumo 1
    // en el otro test ese el valor actual ahora sera +1

    // esto pasaria si se esta reutilizando const wrapper = shallow( <CounterApp /> )
    // como variable global dentro de describe

    // pero podemos reutilzar esa variable sin que cambie el estado
    // let wrapper = shallow( <CounterApp /> )
    // esta funcion se ejecutara antes de cada test
    // beforeEach(()=>{
    //     wrapper = shallow( <CounterApp /> )
    // })


    test('Debe de colocar el valor por defecto con el boton de reset', () => {
        const wrapper = shallow( <CounterApp value={105}/> )

        // const btnAdd = wrapper.find('button').at(0)
        // console.log("🚀 ~ file: CounterApp.test.js ~ line 30 ~ test ~ btnAdd.html()", btnAdd.html())
        // console.log(wrapper.find('button').at(2).html())
        wrapper.find('button').at(0).simulate('click')
        wrapper.find('button').at(0).simulate('click')
        wrapper.find('button').at(2).simulate('click')
        wrapper.find('button').at(2).simulate('click')
        wrapper.find('button').at(2).simulate('click')
        wrapper.find('button').at(2).simulate('click')
        wrapper.find('button').at(1).simulate('click')

        const counterText = wrapper.find('h2').text().trim()
        expect(+counterText).toBe(105)
    })
})
