// encargado de para el xml de jsx
import React from 'react'

// encargado para hacer el render
import ReactDOM from 'react-dom'

// import FirstApp from './FirstApp'

import CounterApp from './CounterApp'
import './index.css';


const saludo = <h1>hello world 1</h1>
console.log("🚀 ~ file: index.js ~ line 5 ~ saludo", saludo)

console.log("hello world")

// ReactDOM.render(saludo,document.getElementById('root'));
// ReactDOM.render(<FirstApp saludo="hola" />,document.getElementById('root'));
// ReactDOM.render(<CounterApp value={ 69 } />,document.getElementById('root'));
ReactDOM.render(<CounterApp value={100} />,document.getElementById('root'));
