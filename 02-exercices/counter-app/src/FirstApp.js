// import React, { Fragment } from "react"
import React from "react"

// sirve para obligar que los componentes utilicen utilicen las propiedades que se
// especificaran ya sea con el tipo de dato y si es requerido
import PropTypes from 'prop-types'

/**
 * existen dos tipos de components en react los que estan basadas en clases y los que estan basadas en 
 * funciones, react recomienda a utilizar componentes con funciones, por la entrada de los
 * hooks
 * 
 * functional components
 * 
 * 
 * antes se llamaba stateless functional component por no podian manejar estado
 * o no manejaban el estado de la manera correcta
 */

// const FirstApp = (props) =>{
// const FirstApp = ( { saludo,subtitulo = "soy un subtitulo" } ) =>{
const FirstApp = ( { saludo,subtitulo = "soy un subtitulo" } ) =>{
    // console.log("🚀 ~ file: FirstApp.js ~ line 17 ~ FirstApp ~ props", props)
    
    const saludo2 = 'Hello world.'
    const obj = {"key":"value"}

    // no es eficiente
    // if(!saludo) throw new Error(
    //     "El saludo es necesario"
    // );
    
    // const {saludo = "por defecto"} = props

    // que problema tenemos con los div
    // es decir si tenemos muchos divs que representan solo los 
    // contenedores de los componentes en algunos casos
    // estos divs solo serviran para agrupar y ensucia el html resultante
    // para eso existen los fragments
    // return (
    //     <div>
    //         <h1>Hello world</h1>
    //         <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, sed. Accusamus quam a deserunt placeat ipsum quaerat iusto voluptate, eius omnis? Ipsa aliquam itaque laboriosam nemo eveniet animi quibusdam vitae?</p>
    //     </div>
    // );

    // Fragment con importacion
    // return (
    //     <Fragment>
    //         <h1>Hello world</h1>
    //         <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, sed. Accusamus quam a deserunt placeat ipsum quaerat iusto voluptate, eius omnis? Ipsa aliquam itaque laboriosam nemo eveniet animi quibusdam vitae?</p>
    //     </Fragment>
    // );
    
    // forma reducida al usar fragments sin usar la importacion
    return (
        <>
            <h1>{ saludo2 }</h1>
            <h1>{ saludo }</h1>
            <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, sed. Accusamus quam
                
             a deserunt placeat ipsum quaerat iusto voluptate, eius omnis? Ipsa aliquam itaque laboriosam nemo eveniet animi quibusdam vitae?</p>
            <p>{[1,2,3,4]}</p>
            {/* los datos booleanos no se mostraran */}
            <p>{true}</p>
            {/* No se puede poner objetos como las demas varibales */}
            <p>{ JSON.stringify(obj)  }</p>  
            <pre>{ JSON.stringify(obj,null,3)  }</pre>  
        </>
    );
}

FirstApp.propTypes = {
    saludo : PropTypes.string.isRequired
}
FirstApp.defaultProps = {
    subtitulo  : "Soy un subtitulo"
}

export {
    FirstApp as default,
}