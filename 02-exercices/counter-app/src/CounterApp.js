// (ES7 React/Redux/GraphQL/React-Native snippets) - rafcp
// https://es.reactjs.org/docs/events.html#mouse-events
// https://es.reactjs.org/docs/handling-events.html
// https://es.reactjs.org/docs/hooks-state.html

import React, { useState } from 'react'
import PropTypes from 'prop-types'

const CounterApp = ({value}) =>{

    const [count,setCount] = useState(value)

    const handleAdd = event=>{
        // console.log("🚀 ~ file: CounterApp.js ~ line 9 ~ CounterApp ~ event", event)
        setCount(count + 1)
        console.log("🚀 ~ file: CounterApp.js ~ line 16 ~ CounterApp ~ count", count)
        console.log("🚀 ~ file: CounterApp.js ~ line 18 ~ CounterApp ~ value", value)
    }

    const handleSubtract = event =>{
        setCount(count - 1)

        // esto nos dice el counter actual va a cambiar
        // si no tener la variable counter
        // setCount(countCurrent=>countCurrent - 1)

    }

    return (
        <>
            <h1>CounterApp</h1>
            <h2>{ count }</h2>
            {/* <button onClick={ function(){console.log('+1')} }>+1</button> */}
            {/* <button onClick={ ()=>{console.log('+1')} }>+1</button> */}
            <button onClick={ handleAdd }>+1</button>
            <button onClick={ handleSubtract }> -1 </button>
        </>
    );
}

CounterApp.propTypes = {
    // value : PropTypes.number.isRequired,
    value : PropTypes.number,
}

CounterApp.defaultProps = {
    value : 0
}


export {
    CounterApp as default,
}
