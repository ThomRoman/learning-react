# **Test Unitarias**

- Ruta critica : la ruta desde cero hasta el fin de un proceso
- react utiliza jest para las pruebas

## **Introduccion a las pruebas unitarias y de integracion**

### **Introducción a las pruebas**

- unitarias : enfocadas en pequeñas funcionalidades, pequeñas piezas de la aplicacion
- integracion : enfocadas en como reaccionan varias piezas en conjunto

### Caracteristicas de las pruebas

- Faciles de escribir
- faciles de leer
- confiables
- rapidas
- principalmente unitarias

### **AAA**

Pasos a seguir para realizar pruebas

#### **Arrage - Arreglar**

Preparacion del estado inicial, el sujero a probar

- Inicializamos variables
- importaciones necesarias

Se prepara el ambiente a probar

#### **Act - Actuar**

Aplicamos aciones o estimulos

- llamar metodos
- simular clicks
- Realizar acciones sobre el paso antenior

#### **Assert - Afirmar**

Observar el comportamiento resultante

- Son los resultados esperado
- Ejm : Que algo cambie, algo incremente o bien que nada sucesa

## Enzyme

Enzyme es una utilidad de prueba de JavaScript para React que 
facilita la prueba de la salida de sus componentes de React. 
También puede manipular, atravesar y, de alguna manera, simular el 
tiempo de ejecución dado el resultado.

La API de Enzyme está destinada a ser intuitiva y flexible al 
imitar la API de jQuery para la manipulación y el recorrido de DOM.

- https://enzymejs.github.io/enzyme/
- https://stackoverflow.com/questions/64658031/which-enzyme-adapter-works-with-react-17
- https://www.npmjs.com/package/@wojtekmaj/enzyme-adapter-react-17

```sh
    npm install --save-dev @wojtekmaj/enzyme-adapter-react-17
    npm install --save enzyme
    npm install --save-dev enzyme-to-json
```

```js
 // CONFIGURATION
//  setupTests.js

import {createSerializer} from 'enzyme-to-json';
import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

Enzyme.configure({ adapter: new Adapter() });
expect.addSnapshotSerializer(createSerializer({mode: 'deep'}));

```