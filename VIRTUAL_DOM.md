# **Virtual DOM**

Es una copia exacta del DOM, cuando se crea un ejemeto con react este lo compora con el DOM del navegador y hace que coincidan. react en memoria
guarda un virtual DOM

ReactDOM obtiene el virtual dom compora con el dom del navegador y trata de coincidir es ahi donde se aplica el render.

Los elementos de react estan en un virtual dom que luego el
metodo render de ReactDOM lo compara con el DOM original e 
intenta igualarlos o los sincroniza, a esto se le conoce como 
**reconciliacion** de tal manera que si encuentra una 
dieferencia en el dom del navegador solo se renderizara ese
elemento que muto

- https://www.youtube.com/watch?v=4gAAiOKOwio
- https://es.reactjs.org/docs/reconciliation.html
- https://es.reactjs.org/docs/faq-internals.html
- https://www.jigsawacademy.com/blogs/data-science/virtual-dom-in-react

## EstadoGlobal de la aplicacion para la comunicacion de componentes

- Redux
- ContextAPI
- MobX

## .

- https://www.youtube.com/watch?v=wHCelLN3lTI
- https://softwareontheroad.com/es/react-hooks/#use-effect
- https://codesandbox.io/s/useeffect-e7d3h?file=/src/App.tsx
- https://codesandbox.io/s/react-cards-wfuct?file=/src/components/CardList.jsx